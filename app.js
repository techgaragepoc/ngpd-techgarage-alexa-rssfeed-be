var express = require("express");
var alexa = require("alexa-app");
var promise = require("promise");


const configuration = require('./json/configuration.json');
const rssUrls = require('./json/rssfeedURLs.json');
const responses = require('./json/commonResponses.json');
const subscribers = require('./json/subscriberdetail.json');


const CommonMethods = require('./lib/commonmethods.js');
const WebScrape = require('./lib/webscrape.js');
const MSIService = require('./lib/msiservice.js');
const MailHandler = require('./lib/googlemailhandler.js');


var IsUserRecognized = false;
var defaultTopNnews = 3;

const APP_ID = configuration.appId;

var PORT = process.env.port || 8080;
var app = express();

// ALWAYS setup the alexa app and attach it to express before anything else.
var alexaApp = new alexa.app("alexaWS");

alexaApp.express({
  expressApp: app,

  // verifies requests come from amazon alexa. Must be enabled for production.
  // You can disable this if you're running a dev environment and want to POST
  // things to test behavior. enabled by default.
  checkCert: false,

  // sets up a GET route when set to true. This is handy for testing in
  // development, but not recommended for production. disabled by default
  debug: true
});

// now POST calls to /test in express will be handled by the app.request() function

// from here on you can setup any other express routes or middlewares as normal
app.set("view engine", "ejs");

function setSessionValues(session,lob,region,articleNo)
{
        if (lob==null || lob == 'undefined' || lob =='')
            lob = 'all';

        if (region==null || region == 'undefined' || region =='')
            region = 'all';

        if (articleNo==null || articleNo == 'undefined' || articleNo =='')
            articleNo = 0;
        
        session.set('news_lob',lob);
        session.set('news_region',region);
        session.set('news_artno',articleNo);
}

function clearPreferences(session) {
    session.set('news_artno','');
    session.set('news_lob','');
    session.set('news_region','');
}

function initiateSession(request)
{

       // check if you can use session (read or write)
    if (request.hasSession())
    {
        // get the session object
        var session = request.getSession();
        var sessionDetail = session.details;
       
        var reqAppId = sessionDetail.application.applicationId;
        var alexaUserId = sessionDetail.user.userId;

        if (reqAppId != configuration.appId) {
            console.log('Invalid Alexa skill trying to call... ' + reqAppId);
            return null;
        }

        //session.set('userId',,alexaUserId);
        var currUserId = session.get('userId');

        console.log('user id identified as => ' + alexaUserId);
        console.log('current user => '+ currUserId);
   
        if (currUserId == null || currUserId == 'undefined' 
                || currUserId == '')
            {
                //session.set('userId',alexaUserId);

                session.set('user','');
                session.set('user_email','');
                session.set('news_artno','');
                session.set('news_lob','');
                session.set('news_region','');
                session.set('news_subs','');
                session.set('news_defTopNnews','3');
                session.set('intent_to_exec','');

                var user = CommonMethods.GetUserByUserId(alexaUserId,subscribers);
                
                if (user) {
                    session.set('userId',alexaUserId);
                    session.set('user',user.name);
                    session.set('user_email',user.email);
                    session.set('news_subs',user.name);
                    IsUserRecognized = true;
                }
    
            }
         
    }
}



function getCard(cardTitle,cardContent) {
    return {
        "type": "Simple",
        "title":cardTitle,
        "content": cardContent
    };
}

function responseToAlexa(response,responsetype,spkresponse,prompt,cardtitle,cardtext) {
       
        var cardObj = getCard(cardtitle,cardtext);
        console.log(cardObj);

        switch (responsetype.toUpperCase()) {
            case 'TELL': return response.say(spkresponse).send(); break;
            case 'ASK': return response.say(spkresponse).reprompt(prompt).shouldEndSession(false).send();break;
            case 'ASKWITHCARD': return response.card(cardtitle,cardtext).say(spkresponse).reprompt(prompt).shouldEndSession(false).send();break;
        }
}

function intent_GetConfirmation(request,response) {
    var session = request.getSession();
    var useremail = session.get('user_email');
    if (useremail == null || useremail == '' || useremail == 'undefined') {
        useremail = 'arun.agrawal@mercer.com';
        session.set('user_email',useremail);
    }

    responseToAlexa(response,'ASK','I will send this information to ' + useremail + ', please confirm?','','','' );
}

function intent_GetMeAllArticlesSendTo(request, response,callback) {
        var session = request.getSession();
        var lob = session.get('news_lob');
        var region = session.get('news_region');
        var sendTo = session.get('news_subs');
        var articleNo = session.get('news_artno');

        var reqdata = {
            news_lob: lob,
            news_region: region,
            news_artno: articleNo,
            news_defTopNnews: session.get('news_defTopNnews')
        };

        MSIService.GetArticles(reqdata,MailHandler,sendTo,function(responsetype,spkresponse,prompt,cardtitle,cardtext){
            callback(responsetype,spkresponse,prompt,cardtitle,cardtext);
        });
}

function intent_GetMeArticlesSendTo(request, response,callback) {
            var session = request.getSession();
            var lob = session.get('news_lob');
            var region = session.get('news_region');
            var sendTo = session.get('news_subs');
            var articleNo = session.get('news_artno');
    
            var reqdata = {
                news_lob: lob,
                news_region: region,
                news_artno: articleNo,
                news_defTopNnews: session.get('news_defTopNnews')
            };
    
            MSIService.GetArticles(reqdata,MailHandler,sendTo,function(responsetype,spkresponse,prompt,cardtitle,cardtext){
                callback(responsetype,spkresponse,prompt,cardtitle,cardtext);
            });
}

alexaApp.pre = function(request, response, type) {
    if (request.applicationId != configuration.appId) {
      // fail ungracefully
      //throw "Invalid applicationId";
      return response.fail("Invalid applicationId");
    }
  };

alexaApp.post = function(request, response, type, exception) {
    console.log('sending repsonse...');
    console.log(response);

    if (exception) {
      // always turn an exception into a successful response
      console.log('exception while sending response...');
      console.log(exception);
       return response.clear().say("An error occured: " + exception).send();
    }
  };  

alexaApp.launch(function(request, response) {
    var msgask = '';
    var msgreprompt = '';
    if (IsUserRecognized)
    {
        msgask = 'Hi ' +  session.get('user')  + ', Welcome to Mercer Select News Service. how can I help you?';
        msgreprompt = msgask;
        
    }
    else {
        msgask = responses["LaunchRequest"].ask;
        msgreprompt = responses["LaunchRequest"].reprompt;
    }
    responseToAlexa(response,'ASK', msgask, msgreprompt,"","");
});

alexaApp.intent("AMAZON.HelpIntent", {},function (request, response) {
    responseToAlexa(response,'ASK',responses["AMAZON.HelpIntent"].ask, responses["AMAZON.HelpIntent"].reprompt,"","");
});

alexaApp.intent("AMAZON.StopIntent", {},function (request, response) {
    responseToAlexa(response,'TELL',responses["NewsServiceStopped"].tell, "","","");
});

alexaApp.intent("AMAZON.CancelIntent", {},function (request, response) {
    responseToAlexa(response,'TELL',responses["NewsServiceStopped"].tell, "","","");
});

alexaApp.intent("SessionEndedRequest", {},function (request, response) {
    responseToAlexa(response,'TELL',responses["NewsServiceStopped"].tell, "","","");
});

alexaApp.sessionEnded(function(request, response) {
    responseToAlexa(response,'TELL',responses["NewsServiceStopped"].tell, "","","");
  });

alexaApp.intent("StartNewsService", {}, function(request, response) {
    responseToAlexa(response,'ASK', responses["NewsServiceStarted"].ask, responses["NewsServiceStarted"].reprompt,"","");
});

alexaApp.intent("StopNewsService", {}, function(request, response) {
    responseToAlexa(response,'TELL',responses["NewsServiceStopped"].tell, "","","");
});

alexaApp.intent("IdentifyUser", {}, function(request, response) {
    var user = request.slot("USER");
    var session = request.getSession();

    if (user)
    {
        session.set('user',user);
        session.set('user_email',CommonMethods.GetEmailId(user,subscribers));
        responseToAlexa(response,'ASK', 'Hi ' + user + ', how can I help you?', 'Hi ' + user + ', how can I help you?','','');
    }
    else {
        responseToAlexa(response,'ASK','Sorry, I could not recognize you. Please try again','','','');
    }
});

alexaApp.intent("SetVoiceNewsCount", {}, function(request, response) {
    var voicenewscount = request.slot("VOICECNT");
    var session = request.getSession();

    session.set('news_defTopNnews',(voicenewscount != null && voicenewscount > 0)?voicenewscount.toString():'3');
    responseToAlexa(response,'ASK','Voice count set as ' + session.get('news_defTopNnews'),'','','');
});

alexaApp.intent("GetConfirmation", {}, function(request, response) {
    
    intent_GetConfirmation(request, response);
});

alexaApp.intent("SetConfirmation", {}, function(request, response) {
    var cnfrmResponse = request.slot("CONSENT");
    var anotherAuthEmailId = request.slot("AUTHEMAIL");
    var session = request.getSession();
    var useremail = session.get('user_email');

    if (cnfrmResponse.toUpperCase() == "YES" || cnfrmResponse.toUpperCase() == "YEP" 
            || cnfrmResponse.toUpperCase() == "YAA" || cnfrmResponse.toUpperCase() == "OK" )
    {
        console.log('mail confirmed to ' + useremail);

        var lob = session.get('news_lob');
        var region = session.get('news_region');
        var sendTo = session.get('news_subs');
        var articleNo = session.get('news_artno');

        var reqdata = {
            news_lob: lob,
            news_region: region,
            news_artno: articleNo,
            news_defTopNnews: session.get('news_defTopNnews')
        };

        if(session.get('intent_to_exec') == 'GetMeArticlesSendTo' ||
            session.get('intent_to_exec') == 'GetMeAllArticlesSendTo') {
           
                                    var intentAction = new promise(function(resolve,reject) {
                                        MSIService.GetArticles(reqdata,MailHandler,sendTo,function(responsetype,spkresponse,prompt,cardtitle,cardtext){
                                            var outputObj = {
                                                responsetype: responsetype,
                                                spkresponse: spkresponse,
                                                reprompt: prompt,
                                                cardtitle: cardtitle,
                                                cardtext: cardtext
                                            };
                                
                                            clearPreferences(session); //to clear last request setting
                                            resolve(outputObj);
                                        });
                                    });
                                
                                    return intentAction.then(function(data){ 
                                            responseToAlexa(response,data.responsetype,data.spkresponse,data.reprompt,data.cardtitle,data.cardtext); 
                                        });
                                 
            }   
        }
        else {
            console.log('another email id');
            console.log(anotherAuthEmailId);
            if (anotherAuthEmailId) {
                    session.set('user_email',anotherAuthEmailId);
                    intent_GetConfirmation(request,response);
            }
            else {
                responseToAlexa(response,'ASK','ok','ok','','');
            }
        
        }
});

alexaApp.intent("GetMeAllArticles", {}, function(request, response) {
    var lob = request.slot("LOB");
    var region = request.slot("REGION");
    var articleNo = request.slot("ARTNO");
    var session = request.getSession();

    initiateSession(request);
    setSessionValues(session,lob,region,articleNo);

    var reqdata = {
        news_lob: session.get('news_lob'),
        news_region: session.get('news_region'),
        news_artno: session.get('news_artno'),
        news_defTopNnews: session.get('news_defTopNnews')
    };

    var intentAction = new promise(function(resolve,reject) {
        MSIService.GetArticles(reqdata,MailHandler,null,function(responsetype,spkresponse,prompt,cardtitle,cardtext){

            var outputObj = {
                responsetype: responsetype,
                spkresponse: spkresponse,
                reprompt: prompt,
                cardtitle: cardtitle,
                cardtext: cardtext
            };

            clearPreferences(session); //to clear last request setting
            resolve(outputObj);
        });
    });

    return intentAction.then(function(data){ 
        responseToAlexa(response,data.responsetype,data.spkresponse,data.reprompt,data.cardtitle,data.cardtext); 
    });
});

alexaApp.intent("GetMeArticles", {}, function(request, response) {
    var lob = request.slot("LOB");
    var region = request.slot("REGION");
    var articleNo = request.slot("ARTNO");
    var voicenewscount = request.slot("VOICECNT");
    var session = request.getSession();

    initiateSession(request);
    //console.log('news_defTopNnews: ' + session.get('news_defTopNnews'));
    var vcount = (voicenewscount != null && voicenewscount != 'undefined' && voicenewscount > 0)?voicenewscount.toString():session.get('news_defTopNnews');
    session.set('news_defTopNnews',vcount);
    setSessionValues(session,lob,region,articleNo);

    var reqdata = {
        news_lob: session.get('news_lob'),
        news_region: session.get('news_region'),
        news_artno: session.get('news_artno'),
        news_defTopNnews: session.get('news_defTopNnews')
    };

    var intentAction = new promise(function(resolve,reject) {
            MSIService.GetArticles(reqdata,MailHandler,null,function(responsetype,spkresponse,prompt,cardtitle,cardtext){

            clearPreferences(session); //to clear last request setting
            var outputObj = {
                responsetype: responsetype,
                spkresponse: spkresponse,
                reprompt: prompt,
                cardtitle: cardtitle,
                cardtext: cardtext
            };
            console.log(outputObj);
            resolve(outputObj);
        });
    });

    return intentAction.then(function(data){ 
        responseToAlexa(response,data.responsetype,data.spkresponse,data.reprompt,data.cardtitle,data.cardtext); 
    });

    //return false;

});

alexaApp.intent("GetMeAllArticlesSendTo", {}, function(request, response) {
        var lob = request.slot("LOB");
        var region = request.slot("REGION");
        var articleNo = request.slot("ARTNO");
        var sendTo = request.slot("ESUBS");
        var session = request.getSession();

        initiateSession(request);
        setSessionValues(session,lob,region,articleNo);

        if (sendTo.toUpperCase() == "ME" || sendTo.toUpperCase() == "MY EMAIL") {
            sendTo =   session.get('user');
        }

        session.set('news_subs',sendTo);
        session.set('intent_to_exec','GetMeAllArticlesSendTo');

        intent_GetConfirmation(request,response);
});

alexaApp.intent("GetMeArticlesSendTo", {}, function(request, response) { 

        var lob = request.slot("LOB");
        var region = request.slot("REGION");
        var articleNo = request.slot("ARTNO");
        var sendTo = request.slot("ESUBS");
        var session = request.getSession();

        initiateSession(request);
        setSessionValues(session,lob,region,articleNo);

        if (sendTo.toUpperCase() == "ME" || sendTo.toUpperCase() == "MY EMAIL") {
            sendTo =   session.get('user');
        }

        session.set('news_subs',sendTo);
        session.set('intent_to_exec','GetMeAllArticlesSendTo');

        intent_GetConfirmation(request,response);
});




app.listen(PORT);
console.log("Listening on port " + PORT + ", try http://localhost:" + PORT + "/alexaWS");