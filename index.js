var Alexa = require('alexa-sdk');
//const https = require('https');
const rssUrls = require('./json/rssfeedURLs.json');
const responses = require('./json/commonResponses.json');
const subscribers = require('./json/subscriberdetail.json');

var WebScrape = require('./lib/webscrape.js');
var CommonMethods = require('./lib/commonmethods.js');
var MailHandler = require('./lib/awsmailhandler.js');

const APP_ID = 'amzn1.ask.skill.325801ec-c00c-4450-b8c2-6614882ab9b9'; //'amzn1.ask.skill.1ea759f1-17c4-4f4b-a449-4953baefbf07';

var IsUserRecognized = false;
var AfterConfirmation = false;
var defaultTopNnews = 3;

//console.log(rssUrls);

function getArticles(self,lob,region,subscriber, cb)
{
        var reqArticleNo = 0;

        //if (!lob) { lob = 'all'; }
       // if (!region) { region = 'all'; }

       var restrictVoiceTo = 3;
       var condition = '';
     
       if (self && self.attributes) {
        lob = self.attributes['news_lob'];
        region = self.attributes['news_region'];
        reqArticleNo = self.attributes['news_artno'];
        restrictVoiceTo = parseInt(self.attributes['news_defTopNnews']);
       }

        for(var iCtr=0; iCtr < rssUrls.length; iCtr++)
            {
                if (lob.toUpperCase() == rssUrls[iCtr].lob.toUpperCase())
                {
                    // Extract some data from my website
                    WebScrape.GetArticles(rssUrls[iCtr].url, {lob: rssUrls[iCtr].lob},
                                function(err, data)  {
                                    if (data)
                                    {
                                    //  console.log('Articles fetched....%s',data[0].lob);
                                    //  console.log(data);
                                    //  console.log('*********************************');
                                    //    console.log(region);
                                       var filteredArticles = [];
                                       for(var rCtr=0; rCtr < data.length; rCtr++) {
                                            //console.log(data[rCtr]);
                                            if (region.toUpperCase() == 'ALL' ||
                                                   region.toUpperCase() == data[rCtr].article.region.toUpperCase() ||
                                                   region.toUpperCase() == data[rCtr].article.subregion.toUpperCase())
                                            {

                                               filteredArticles.push(data[rCtr]);
                                            }
                                       }


                                        var newsAsString = ArticlesAsString(filteredArticles,reqArticleNo,restrictVoiceTo);

                                        if (reqArticleNo > 0)
                                            condition = 'SPECIFIC_ARTICLE';

                                        cb(null,condition,data[0].lob,region,newsAsString.titles,newsAsString.titlesAnddescription,subscriber,self);
                                        return;
                                    }
                                    else
                                    {
                                        console.log(err);
                                        cb(err,condition,'',null,null,null,null,self);
                                        return;
                                    }
                                });
                }

            }

}

function ArticlesAsString(articles,reqArticleNo,restrictVoiceTo)
{
    var news = {titles: '', titlesAnddescription: ''};
    var linebreak = '\t\r\n';
    var newsCtr = 0;

    if (articles && articles.length > 0)
    {
        for(var i=0; i< articles.length; i++)
        {
            if (reqArticleNo == 0 || reqArticleNo == (i+1))
            {
                //console.log(articles[i]);
                if (newsCtr < restrictVoiceTo) {

                    //for specific article, add description also
                    if (reqArticleNo > 0) {
                        news.titles +=  "Article " + ((i+1).toString() + "<break time='500ms'/>" + articles[i].article.title + "<break time='1s'/>" + articles[i].article.description + "<break time='2s'/>");
                    }
                    else {
                        news.titles +=  ((i+1).toString() + "<break time='500ms'/>" + articles[i].article.title + linebreak  + "<break time='2s'/>");
                    }

                }

                news.titlesAnddescription +=  ((i+1).toString() + "." + articles[i].article.title + ": " + articles[i].article.description + linebreak + linebreak);
                newsCtr++;
            }
        }
    }

    news.titles = "<break time='1s'/>" + news.titles + " End of news <break time='2s'/>";

    return news;
}

//to test
/*
var testlob = 'all'; //'health', 'wealth','career'
var testregion = 'all';

//setSessionValues(this,testlob,testregion,null);
getArticles(this,testlob,testregion,null,function(err,cond,lob,region,titleOnly,titleContent,subs,obj){
            console.log(testlob);console.log(titleContent);console.log('********************');});

/*     
var recipients = [];
var emailContent = "1.Medicaid clash jeopardizes GOP Senate health deal\t\r\n2.How workers could actually get paid parental leave \t\r\n3.More in Hong Kong enrolling in health insurance\t\r\n4.Japan considers increasing workers' paid leave\t\r\n5.Beware the ill winds of Canada's paid sick leave \t\r\n";
recipients.push(CommonMethods.GetEmailId('arun',subscribers));
MailHandler.SendMail(recipients,'test subject',emailContent,function(err,data){
    console.log('callback called');
    console.log(err);
    console.log(data);
});
*/


function SendResponse(err,condition,lob,region,titleOnly,titleContent,subscriber,obj)
 {

        if (lob && lob.toUpperCase() == 'ALL') {lob = ''; }
        if (region && region.toUpperCase() == 'ALL') {region = ''; } else { region = " for " + region;}

        var titleOutput = lob + " News " + region;

        clearPreferences(obj); //to clear last request setting

        if (titleContent)
        {
            if (subscriber)
            {
                var recipients = [];
                titleOutput = "Alexa Response: " + titleOutput;

                recipients.push(CommonMethods.GetEmailId(subscriber,subscribers));
                var mailContent = "Hi, \t\r\n \t\r\n As requested, news are \t\r\n \t\r\n" + titleContent;
                MailHandler.SendMail(recipients,titleOutput,mailContent, function(err,data){

                    if (data) {
                        titleOutput += data;
                        obj.emit(":askWithCard", data, "Anything else you would like to know?",titleOutput, titleContent);
                    }
                    else {
                        titleOutput += "..error occured while sending mail to " + CommonMethods.GetEmailId(subscriber,subscribers) + ".." + err;
                        obj.emit(":askWithCard", titleOutput, "Anything else you would like to know?",titleOutput, titleContent);
                    }

                   // obj.emit(":ask", "Anything else you would like to know?");
                });

            }
            else {

                if (condition == 'SPECIFIC_ARTICLE') {
                    titleOutput = titleOnly;
                }
                else {
                    titleOutput += (" are " + titleOnly);
                }

                obj.emit(":askWithCard", titleOutput, "Anything else you would like to know?",titleOnly, titleContent);

              //  obj.emit(":ask", "Anything else you would like to know?");
            }
        }
        else
        {
            obj.emit(":ask", "Sorry, I am not able to find requested information. Please try again <break time='3s'/>");
        }
 }


function setSessionValues(obj,lob,region,articleNo)
{
        obj.attributes['news_lob'] = lob?lob:((obj.attributes['news_lob'] && obj.attributes['news_lob'] != '')?obj.attributes['news_lob']:'all');
        obj.attributes['news_region'] = region?region:((obj.attributes['news_region'] && obj.attributes['news_region'] != '')?obj.attributes['news_region']:'all');
        obj.attributes['news_artno'] = articleNo?articleNo:'0';
}

function clearPreferences(obj) {
    obj.attributes['news_artno'] = '';
    obj.attributes['news_lob'] = '';
    obj.attributes['news_region'] = '';
}


function initiateSession(obj)
 {

    var currUserId = obj.attributes['userId'];
    
    if (currUserId == null || currUserId == 'undefined' 
            || currUserId == '')
        {

            obj.attributes['user'] = '';
            obj.attributes['user_email'] = '';
            obj.attributes['news_artno'] = '';
            obj.attributes['news_lob'] = '';
            obj.attributes['news_region'] = '';
            obj.attributes['news_subs'] = '';
            obj.attributes['news_defTopNnews'] = '3';
            obj.attributes['intent_to_exec'] = '';

            console.log(obj.event.session.user.userId);
            var user = CommonMethods.GetUserByUserId(obj.event.session.user.userId,subscribers);

            if (user) {
                obj.attributes['userId'] = user.alexaUserId;
                obj.attributes['user'] = user.name;
                obj.attributes['user_email'] = user.email;
                
                IsUserRecognized = true;
            }
        }
 }


const handlers = {

    'LaunchRequest': function () {
       // this.emit(':ask', responses["LaunchRequest"].ask, responses["LaunchRequest"].reprompt);
       if (IsUserRecognized)
        {
            var welcomeMsg = 'Hi ' +  this.attributes['user']  + ', Welcome to Mercer Select News Service. how can I help you?';
            this.emit(':ask', welcomeMsg, welcomeMsg);
        }
        else {
            this.emit(':ask', responses["LaunchRequest"].ask, responses["LaunchRequest"].reprompt);
        }
    },
    'AMAZON.HelpIntent': function () {
        this.emit(':ask', responses["AMAZON.HelpIntent"].ask, responses["AMAZON.HelpIntent"].reprompt);
    },
    'AMAZON.StopIntent': function () {
        this.emit(':tell', responses["AMAZON.StopIntent"].tell);
    },
    'AMAZON.CancelIntent': function () {
        this.emit('AMAZON.StopIntent');
    },
    'SessionEndedRequest': function () {
        this.emit('AMAZON.StopIntent');
    },
    'Unhandled': function () {
        this.emit('AMAZON.HelpIntent');
    },

    'NewSession1001': function () {
      //  this.handler.state = '';
        this.attributes['user'] = '';
        this.attributes['user_email'] = '';
        this.attributes['news_artno'] = '';
        this.attributes['news_lob'] = '';
        this.attributes['news_region'] = '';
        this.attributes['news_subs'] = '';
        this.attributes['news_defTopNnews'] = '3';
        this.attributes['intent_to_exec'] = '';

        console.log(this.event.session.user.userId);
        var user = CommonMethods.GetUserByUserId(this.event.session.user.userId,subscribers);

        if (user) {
            this.attributes['userId'] = user.alexaUserId;
            this.attributes['user'] = user.name;
            this.attributes['user_email'] = user.email;
            
            IsUserRecognized = true;
        }

        this.emit('LaunchRequest');
    },
    'StartNewsService': function () {  //invoke statement: ask Mercer to start news service

        this.emit(':ask', responses["NewsServiceStarted"].ask, responses["NewsServiceStarted"].reprompt);
    },
    'StopNewsService': function () {
        this.emit(':tell', responses["NewsServiceStopped"].tell);
    },
    'IdentifyUser': function () {  //invoke statement: I am Arun

        var user = this.event.request.intent.slots.USER.value;

        if (user)
        {
            this.attributes['user'] = user;
            this.attributes['user_email'] = CommonMethods.GetEmailId(user,subscribers);
            this.emit(':ask', 'Hi ' + user + ', how can I help you?', 'Hi ' + user + ', how can I help you?');
        }
        else {
            this.emit(':ask','Sorry, I could not recognize you. Please try again');
        }

    },

    'SetVoiceNewsCount' : function() {
        var voicenewscount = this.event.request.intent.slots.VOICECNT.value;

        this.attributes['news_defTopNnews'] = (voicenewscount != null && voicenewscount > 0)?voicenewscount.toString():'3';
        this.emit(':ask','Voice count set as ' + this.attributes['news_defTopNnews'] );
    },

    'GetConfirmation' : function() {

        if (this.attributes['user_email'] == null || this.attributes['user_email'] == '' || this.attributes['user_email'] == 'undefined') {
            this.attributes['user_email'] = 'arun.agrawal@mercer.com';
        }

        this.emit(':ask','I will send this information to ' + this.attributes['user_email'] + ', please confirm?' );
    },

    'SetConfirmation' : function() {
        var cnfrmResponse = this.event.request.intent.slots.CONSENT.value;
        var anotherAuthEmailId = this.event.request.intent.slots.AUTHEMAIL.value;

        if (cnfrmResponse.toUpperCase() == "YES" || cnfrmResponse.toUpperCase() == "YEP" 
                || cnfrmResponse.toUpperCase() == "YAA" || cnfrmResponse.toUpperCase() == "OK" )
        {
            console.log('mail confirmed to ' + this.attributes['user_email']);
            AfterConfirmation = true;
            this.emit(this.attributes['intent_to_exec']);   
        }
        else {
            console.log('another email id');
            console.log(anotherAuthEmailId);
            if (anotherAuthEmailId) {
                    this.attributes['user_email'] = anotherAuthEmailId;
                    this.emit('GetConfirmation');
            }
            else {
                AfterConfirmation = false;
                this.emit(':ask','ok');
            }
           
        }
    },

    'GetMeAllArticles': function () {
        var lob = this.event.request.intent.slots.LOB.value;
        var region = this.event.request.intent.slots.REGION.value;
        var articleNo = this.event.request.intent.slots.ARTNO.value;

        initiateSession(this);

        setSessionValues(this,lob,region,articleNo);

        getArticles(this,lob,region,null,SendResponse);
    },
    'GetMeArticles': function () {
        var lob = this.event.request.intent.slots.LOB.value;
        var region = this.event.request.intent.slots.REGION.value;
        var articleNo = this.event.request.intent.slots.ARTNO.value;
        var voicenewscount = this.event.request.intent.slots.VOICECNT.value;

        initiateSession(this);

        this.attributes['news_defTopNnews'] = (voicenewscount != null && voicenewscount > 0)?voicenewscount.toString():this.attributes['news_defTopNnews'];
        setSessionValues(this,lob,region,articleNo);
        getArticles(this,lob,region,null,SendResponse);
    },
    'GetMeAllArticlesSendTo': function () {

        if (!AfterConfirmation) {
            var lob = this.event.request.intent.slots.LOB.value;
            var region = this.event.request.intent.slots.REGION.value;
            var sendTo = this.event.request.intent.slots.ESUBS.value;
            var articleNo = this.event.request.intent.slots.ARTNO.value;

            initiateSession(this);
            setSessionValues(this,lob,region,articleNo);

            if (sendTo.toUpperCase() == "ME" || sendTo.toUpperCase() == "MY EMAIL") {
                sendTo =   this.attributes['user'];
            }

            this.attributes['news_subs'] = sendTo;
            this.attributes['intent_to_exec'] = 'GetMeAllArticlesSendTo';

            this.emit('GetConfirmation');

        }
        else {

            var lob = this.attributes['news_lob'];
            var region = this.attributes['news_region'];
            var sendTo = this.attributes['news_subs'];
            var articleNo = this.attributes['news_artno'];

            AfterConfirmation = false;
            getArticles(this,lob,region,sendTo,SendResponse);
        }
        
    },
    'GetMeArticlesSendTo': function () {

          if (!AfterConfirmation) {
                var lob = this.event.request.intent.slots.LOB.value;
                var region = this.event.request.intent.slots.REGION.value;
                var sendTo = this.event.request.intent.slots.ESUBS.value;
                var articleNo = this.event.request.intent.slots.ARTNO.value;

                initiateSession(this);
                setSessionValues(this,lob,region,articleNo);

                if (sendTo.toUpperCase() == "ME" || sendTo.toUpperCase() == "MY EMAIL") {
                    sendTo =   this.attributes['user'];
                }

                this.attributes['news_subs'] = sendTo;
                this.attributes['intent_to_exec'] = 'GetMeArticlesSendTo';

                this.emit('GetConfirmation');
          }
        else {
            
            var lob = this.attributes['news_lob'];
            var region = this.attributes['news_region'];
            var sendTo = this.attributes['news_subs'];
            var articleNo = this.attributes['news_artno'];

            AfterConfirmation = false;
            getArticles(this,lob,region,sendTo,SendResponse);

        }

    }
};

exports.handler = function (event, context, callback) {
    var alexa = Alexa.handler(event, context);
    alexa.appId = APP_ID;
    alexa.registerHandlers(handlers);
    alexa.execute();
};
