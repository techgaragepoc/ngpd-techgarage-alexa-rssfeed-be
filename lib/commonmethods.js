

function escapeRegExp(str) {
    return str.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");
}

module.exports = {

  SearchStringInArray: function(str, strArray) {
                            for (var j=0; j<strArray.length; j++) {
                                if (strArray[j].match(str)) return j;
                            }
                            return -1;
                        },


  ReplaceAll:          function(str, find, replace) {
                          return str.replace(new RegExp(escapeRegExp(find), 'g'), replace);
                      },

  GetEmailId:         function (subs,subscribers)
                          {
                            for(var sCtr = 0;sCtr < subscribers.length; sCtr++)
                            {
                                if (subs.toUpperCase() == subscribers[sCtr].name.toUpperCase())
                                {
                                   return subscribers[sCtr].email;
                                }
                            }

                            return '';
                          },     

GetTitle:              function(titles)
                        {
                            var title = '';

                            if (titles) {
                                for(var i=0;i<titles.length;i++)
                                    {
                                        title += (titles[i]);
                                        if (i < (titles.length-1))
                                        title += '|';
                                    }
                            }
                            return title;    
                        },     
                        
GetUserByUserId:        function (userid,subscribers)
                        {
                            for(var sCtr = 0;sCtr < subscribers.length; sCtr++)
                            {
                                if (userid == subscribers[sCtr].alexaUserId)
                                {
                                    return subscribers[sCtr];
                                }
                            }

                            return null;

                        },

                        

}
