// Import the dependencies
const req = require("request")
     ,FeedParser  = require('feedparser')
     ,Readable = require('stream').Readable
     ,CommonMethods = require('./commonmethods.js')
     ,striptags = require('striptags');
     
function getRegionTaxonomy(taxonomyitems){
   
   var result = {region: '',subregion: ''};
   // console.log(taxonomyitems);
    if (taxonomyitems)
    {
        for(var iCtr=0;iCtr < taxonomyitems.length; iCtr++) {
           var taxonomy = taxonomyitems[iCtr]['@'];
            if (taxonomy && taxonomy.taxtype == 'G')
             {
                 return {
                   region: taxonomy.taxpathname,
                   subregion: taxonomy.taxname    
                 };
             }
        }
    }

   return result;
}

function replaceUnwantedChars(source) {

    var filterString = source;
    filterString =  CommonMethods.ReplaceAll(filterString,'&nbsp;',' ');
    filterString =  CommonMethods.ReplaceAll(filterString,'</p>','');
    filterString =  CommonMethods.ReplaceAll(filterString,'<br>','');
    filterString =  CommonMethods.ReplaceAll(filterString,'&amp;','&');

    //sepcific to speak
    filterString =  striptags(filterString,['<p>']);
    filterString =  CommonMethods.ReplaceAll(filterString,'<p>',"<break time='500ms'/>");
    filterString =  CommonMethods.ReplaceAll(filterString,'\n',"<break time='500ms'/>");
    filterString =  CommonMethods.ReplaceAll(filterString,'&'," and ");

    return filterString;
}

module.exports = {

// Define the scrape function
GetArticles: function (url, data,cb) {

                                var articles = [];
                                // 1. Create the request

                                var options = {
                                    url: url,
                                    "rejectUnauthorized": false, 
                                };

                               // var feedparser = new FeedParser();
                            
                            
                            req.get(options, 
                                        function (err, response, body) {
                                
                                            if (err) { return cb(err,null); }
                                
                                            var memstream = new Readable;
                                            memstream.push(body.toString())    // the string you want
                                            memstream.push(null)               // indicates end-of-file basically - the end of the stream
                                        
                                            // 2. Parse the rdf feed
                                            memstream.pipe(new FeedParser())
                                            // 3. Extract the data
                                            .on('readable', function() {
                                                var stream = this, item;
                                                while (item = stream.read()) {
                                                     
                                                  //  console.log('Got article:');
                                                   // console.log(item);
                                                  // console.log('>>>>>>>>>>>>>>>>>');
                                                   //console.log(item["rdf:taxonomyitems"].taxonomyitem);
                                                  //   console.log(item["rdf:taxonomyitems"].taxonomyitem[0]['@'])
                                                  //  console.log('<<<<<<<<<<<<<<');
                                                  //  console.log(item.meta["rdf:items"]);
                                                  
                                                    var regiontaxonomy = getRegionTaxonomy(item["rdf:taxonomyitems"].taxonomyitem);
                                                    
                                                    var newsTitle = replaceUnwantedChars(item.title);
                                                    var newsDesc = replaceUnwantedChars(item.description);

                                                        articles.push({lob: data.lob, 
                                                            article: {title: newsTitle
                                                                    ,description: newsDesc
                                                                    ,pubdate: item.pubdate
                                                                    ,link: item.link
                                                                    ,region: regiontaxonomy.region
                                                                    ,subregion: regiontaxonomy.subregion
                                                                   // ,summary: item.summary 
                                                                    }
                                                                    });
                                                    }
                                            })
                                            // Send the data in the callback
                                            .on('end', function() {
                                                //  console.log('ended') // the end event will be called properly
                                                //  console.log(articles);
                                                    cb(null, articles);
                                            });


                                    });

                            }
};

