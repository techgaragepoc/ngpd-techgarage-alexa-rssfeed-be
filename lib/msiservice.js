const rssUrls = require('../json/rssfeedURLs.json');
const responses = require('../json/commonResponses.json');
const subscribers = require('../json/subscriberdetail.json');

var WebScrape = require('./webscrape.js');
var CommonMethods = require('./commonmethods.js');


function GetRSSforLOB(lob) {

    for(var iCtr=0; iCtr < rssUrls.length; iCtr++)
    {
        if (lob.toUpperCase() == rssUrls[iCtr].lob.toUpperCase())
        {
            return rssUrls[iCtr];
        }
    }

    return null;
}

function ArticlesAsString(articles,reqArticleNo,restrictVoiceTo)
{
    var news = {titles: '', titlesAnddescription: ''};
    var linebreak = '\t\r\n';
    var newsCtr = 0;

    if (articles && articles.length > 0)
    {
        for(var i=0; i< articles.length; i++)
        {
            if (reqArticleNo == 0 || reqArticleNo == (i+1))
            {
                //console.log(articles[i]);
                if (newsCtr < restrictVoiceTo) {

                    //for specific article, add description also
                    if (reqArticleNo > 0) {
                        news.titles +=  "Article " + ((i+1).toString() + "<break time='500ms'/>" + articles[i].article.title + "<break time='1s'/>" + articles[i].article.description + "<break time='2s'/>");
                    }
                    else {
                        news.titles +=  ((i+1).toString() + "<break time='500ms'/>" + articles[i].article.title + linebreak  + "<break time='2s'/>");
                    }

                }

                news.titlesAnddescription +=  ((i+1).toString() + "." + articles[i].article.title + ": " + articles[i].article.description + linebreak + linebreak);
                newsCtr++;
            }
        }
    }

    news.titles = "<break time='1s'/>" + news.titles + " End of news <break time='2s'/>";

    return news;
}

function SendResponse(err,condition,lob,region,titleOnly,titleContent,mailhandler,subscriber,callback)
 {

        if (lob && lob.toUpperCase() == 'ALL') {lob = ''; }
        if (region && region.toUpperCase() == 'ALL') {region = ''; } else { region = " for " + region;}

        var titleOutput = lob + " News " + region;

        if (titleContent)
        {
            if (subscriber)
            {
                var recipients = [];
                var user_email = CommonMethods.GetEmailId(subscriber,subscribers);
                titleOutput = "Alexa Response: " + titleOutput;

                recipients.push(user_email);
                var mailContent = "Hi, \t\r\n \t\r\n As requested, news are \t\r\n \t\r\n" + titleContent;
                mailhandler.SendMail(recipients,titleOutput,mailContent, function(err,data){

                    if (data) {
                        titleOutput += data;
                        //obj.emit(":askWithCard", data, "Anything else you would like to know?",titleOutput, titleContent);
                        callback("ASKWITHCARD", data, "Anything else you would like to know?",titleOutput, titleContent);
                    }
                    else {
                        titleOutput += "..error occured while sending mail to " + user_email + ".." + err;
                        //obj.emit(":askWithCard", titleOutput, "Anything else you would like to know?",titleOutput, titleContent);
                        callback("ASKWITHCARD", titleOutput, "Anything else you would like to know?",titleOutput, titleContent);
                    }

                   // obj.emit(":ask", "Anything else you would like to know?");
                });

            }
            else {

                if (condition == 'SPECIFIC_ARTICLE') {
                    titleOutput = titleOnly;
                }
                else {
                    titleOutput += (" are " + titleOnly);
                }

                //obj.emit(":askWithCard", titleOutput, "Anything else you would like to know?",titleOnly, titleContent);
                callback("ASKWITHCARD", titleOutput, "Anything else you would like to know?",titleOnly, titleContent);

              //  obj.emit(":ask", "Anything else you would like to know?");
            }
        }
        else
        {
            //obj.emit(":ask", "Sorry, I am not able to find requested information. Please try again <break time='3s'/>");
            callback("ASK", "Sorry, I am not able to find requested information. Please try again <break time='3s'/>","","","");
        }
 }




module.exports = {

    GetArticles:  function (requestdata,mailhandler,subscriber,callback)
                    {
                        var reqArticleNo = 0;
                        var restrictVoiceTo = 3;
                        var condition = '';
                        var lob = '';
                        var region = '';
                        
                        if (requestdata) {
                            console.log(requestdata);

                            lob = requestdata.news_lob;
                            region = requestdata.news_region;
                            reqArticleNo = requestdata.news_artno;
                            restrictVoiceTo = parseInt(requestdata.news_defTopNnews);
                        

                            var lobRSS = GetRSSforLOB(lob);

                            if (lobRSS)
                                {
                                    // Extract some data from my website
                                    WebScrape.GetArticles(lobRSS.url, {lob: lobRSS.lob},
                                                function(err, data)  {
                                                    if (data)
                                                    {
                                                            var filteredArticles = [];
                                                            for(var rCtr=0; rCtr < data.length; rCtr++) {
                                                                    //console.log(data[rCtr]);
                                                                    if (region.toUpperCase() == 'ALL' ||
                                                                        region.toUpperCase() == data[rCtr].article.region.toUpperCase() ||
                                                                        region.toUpperCase() == data[rCtr].article.subregion.toUpperCase())
                                                                    {
                        
                                                                    filteredArticles.push(data[rCtr]);
                                                                    }
                                                        }
                
                
                                                        var newsAsString = ArticlesAsString(filteredArticles,reqArticleNo,restrictVoiceTo);
                
                                                        if (reqArticleNo > 0) {
                                                            condition = 'SPECIFIC_ARTICLE';
                                                        }
                
                                                            SendResponse(null,condition,data[0].lob,region,newsAsString.titles,newsAsString.titlesAnddescription,mailhandler,subscriber,callback);
                                                        
                                                    }
                                                    else
                                                    {
                                                        console.log(err);
                                                        SendResponse(err,condition,'',null,null,null,mailhandler,null,callback);
                                                        
                                                    }
                                                });
                                }
                        }
                    
                    }

}